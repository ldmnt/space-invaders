def calculate_initial_position(entity, screen_dimension):

    screen_w, screen_h = screen_dimension
    sp_w, sp_h = entity.sprite.get_width(), entity.sprite.get_height()
    entity_x1, entity_y1 = screen_w // 2 - sp_w // 2, 0

    if entity.name == "player":
        entity_y1 = (screen_h - screen_h / 6) - (sp_h / 2)
    if entity.name == "invader":
        entity_y1 = screen_h - 5 * (screen_h / 6) - (sp_h / 2)
    if entity.name == "missile":
        entity_y1 = (screen_h - screen_h / 2) - (sp_h / 2)

    position = (entity_x1, entity_x1 + sp_w), (entity_y1, entity_y1 + sp_h)
    entity = entity._replace(position=position)

    return entity


def initial_positions(entities, screen_dimension):

    for entity in entities:

        if entity.name == "player":
            entity = calculate_initial_position(entity, screen_dimension)
            entities = entities._replace(player=entity)

        if entity.name == "invader":
            entity = calculate_initial_position(entity, screen_dimension)
            entities = entities._replace(invader=entity)

        if entity.name == "missile":
            entity = calculate_initial_position(entity, screen_dimension)
            entities = entities._replace(missile=entity)

    return entities


def two_dir(direction_one, direction_two, entity, speed):
    if direction_one and not direction_two:
        entity = entity._replace(move=-speed)
    if direction_two and not direction_one:
        entity = entity._replace(move=speed)
    if not direction_one and not direction_two:
        entity = entity._replace(move=0)
    return entity


def four_dir(directions, entity, speed):
    one, two, three, four = directions
    if one and not two:
        entity = entity._replace(move=(-speed, entity.move[1]))
    if two and not one:
        entity = entity._replace(move=(speed, entity.move[1]))
    if not one and not two:
        entity = entity._replace(move=(0, entity.move[1]))
    if three and not four:
        entity = entity._replace(move=(entity.move[0], -speed))
    if four and not three:
        entity = entity._replace(move=(entity.move[0], speed))
    if not three and not four:
        entity = entity._replace(move=(entity.move[0], 0))
    return entity


def directions(input_state, entities):

    for entity in entities:

        if entity.name == "player":
            entity = two_dir(input_state.left, input_state.right, entity, 0.3)
            entities = entities._replace(player=entity)

        if entity.name == "invader":
            if entity.move == "m":
                move = 0, 0
                entity = entity._replace(move=move)
            directions = (
                input_state.inv_left,
                input_state.inv_right,
                input_state.inv_up,
                input_state.inv_down,
            )
            entity = four_dir(directions, entity, 0.3)
            entities = entities._replace(invader=entity)

        if entity.name == "missile":
            entity = two_dir(input_state.up, input_state.down, entity, 0.3)
            entities = entities._replace(missile=entity)

    return entities


def boundaries(left_boundary, right_boundary, x1):
    if x1 <= left_boundary:
        x1 = left_boundary
    elif x1 >= right_boundary:
        x1 = right_boundary
    return x1


def change_position(entity):
    x1 = entity.position[0][0] + entity.move[0]
    x2 = x1 + entity.sprite.get_width()
    y1 = entity.position[1][0] + entity.move[1]
    y2 = y1 + entity.sprite.get_height()
    return (x1, x2), (y1, y2)


def calculate_position(entities, screen_dim):

    for entity in entities:

        if entity.name == "player":
            x1 = entity.position[0][0] + entity.move
            entity_width = entity.sprite.get_width()
            boundary = screen_dim[0] - entity_width
            x1 = boundaries(0, boundary, x1)
            position = (x1, x1 + entity_width), entity.position[1]
            entity = entity._replace(position=position)
            entities = entities._replace(player=entity)

        if entity.name == "invader":
            entity = entity._replace(position=change_position(entity))
            entities = entities._replace(invader=entity)

        if entity.name == "missile":
            mis_x1, mis_x2 = entity.position[0]
            mis_x1 += entities.player.move
            mis_w = entity.sprite.get_width()
            pl_w = entities.player.sprite.get_width()
            left_boundary = pl_w / 2 - mis_w / 2
            right_boundary = screen_dim[0] - pl_w / 2 - mis_w / 2
            mis_x1 = boundaries(left_boundary, right_boundary, mis_x1)
            mis_x2 = mis_x1 + entity.sprite.get_width()
            mis_y1, mis_y2 = entity.position[1]
            mis_y1 += entity.move
            mis_y2 = mis_y1 + entity.sprite.get_height()
            position = (mis_x1, mis_x2), (mis_y1, mis_y2)
            entity = entity._replace(position=position)
            entities = entities._replace(missile=entity)

    return entities


def moves(input_state, entities, screen_dim):
    entities = directions(input_state, entities)
    entities = calculate_position(entities, screen_dim)
    return entities
