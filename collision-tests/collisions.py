def is_collision(attacker, target):
    (a_x1, a_x2), (a_y1, a_y2) = attacker.position
    (t_x1, t_x2), (t_y1, t_y2) = target.position
    return (
        (t_x1 <= a_x1 <= t_x2 or t_x1 <= a_x2 <= t_x2)
        and ( t_y1 <= a_y1 <= t_y2 or t_y1 <= a_y2 <= t_y2)
    )

def check_collisions(entities):
    if is_collision(entities.invader, entities.player):
        print("game_over")
    else:
        print("—————————")
    if is_collision(entities.missile, entities.invader):
        print("1 point !")
    else:
        print("—————————")
