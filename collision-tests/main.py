from collections import namedtuple
import pygame
import visual
import entities as ent
import state
import collisions

pygame.init()

GENERAL_PATH = "w_assets/"

BACKGROUND_PATH = GENERAL_PATH + "backgrounds/space.jpg"
BACKGROUND = pygame.image.load(BACKGROUND_PATH)
SCREEN_DIMENSION = BACKGROUND.get_width(), BACKGROUND.get_height()
SCREEN = visual.screen(SCREEN_DIMENSION, "Test de collision")
GAME_FRAME = SCREEN, BACKGROUND

SPRITES_PATH = GENERAL_PATH + "sprites/"
PL_SP = pygame.image.load(SPRITES_PATH + "player.png")
INV_SP = pygame.image.load(SPRITES_PATH + "invader.png")
MIS_SP = pygame.image.load(SPRITES_PATH + "missile.png")

Entity = namedtuple("entity", ["name", "sprite", "position", "move"])
Entities = namedtuple("entities", ["player", "invader", "missile"])
InputState = namedtuple(
    "inputs_state",
    [
        "run",
        "left",
        "right",
        "up",
        "down",
        "inv_left",
        "inv_right",
        "inv_up",
        "inv_down",
    ],
)


def main():
    player = Entity(name="player", sprite=PL_SP, position="p", move="m")
    invader = Entity(name="invader", sprite=INV_SP, position="p", move="m")
    missile = Entity(name="missile", sprite=MIS_SP, position="p", move="m")
    entities = Entities(player=player, invader=invader, missile=missile)
    entities = ent.initial_positions(entities, SCREEN_DIMENSION)
    input_state = InputState(
        run=True,
        left=False,
        right=False,
        up=False,
        down=False,
        inv_left=False,
        inv_right=False,
        inv_up=False,
        inv_down=False,
    )
    while input_state.run:
        input_state = state.get_input(input_state)
        entities = ent.moves(input_state, entities, SCREEN_DIMENSION)
        collisions.check_collisions(entities)
        visual.draw_game(GAME_FRAME, entities)


if __name__ == "__main__":
    main()
