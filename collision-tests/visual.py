import pygame


def screen(screen_dimension, title):
    x, y = screen_dimension[0], screen_dimension[1]
    pygame.display.set_caption(title)
    return pygame.display.set_mode((x, y))


def background(screen, background):
    screen.blit(background, (0, 0))


def draw_sprite(screen, entity):
    x, y = entity.position[0][0], entity.position[1][0]
    screen.blit(entity.sprite, (x, y))


def draw_game(game_frame, entities):
    screen, bground = game_frame
    background(screen, bground)
    for entity in entities:
        if entity.name == "player":
            draw_sprite(screen, entity)
        if entity.name == "invader":
            draw_sprite(screen, entity)
        if entity.name == "missile":
            draw_sprite(screen, entity)
    pygame.display.update()
