# Space invaders



## Launch

./y_launch

## Move entities

Player : right and left arrows

Missile : up and down arrows

Invader : x(left), .(right), q(up), g(down) — bépo keyboard…

You can change the keymappings in the "state.py" file using the documentation here :

https://www.pygame.org/docs/ref/key.html

## Output

Print "game_over" if there is a collision between the player and the invader,

print "1 point !" if there is a collision between the missile and the invader

or print "—————————" if there is no collision.
