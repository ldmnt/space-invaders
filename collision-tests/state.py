# pylint: disable=too-many-branches

import pygame


def get_input(in_state):

    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            in_state = in_state._replace(run=False)

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                in_state = in_state._replace(left=True)
            if event.key == pygame.K_RIGHT:
                in_state = in_state._replace(right=True)
            if event.key == pygame.K_UP:
                in_state = in_state._replace(up=True)
            if event.key == pygame.K_DOWN:
                in_state = in_state._replace(down=True)
            if event.key == pygame.K_x:
                in_state = in_state._replace(inv_left=True)
            if event.key == pygame.K_PERIOD:
                in_state = in_state._replace(inv_right=True)
            if event.key == pygame.K_q:
                in_state = in_state._replace(inv_up=True)
            if event.key == pygame.K_g:
                in_state = in_state._replace(inv_down=True)

        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                in_state = in_state._replace(left=False)
            if event.key == pygame.K_RIGHT:
                in_state = in_state._replace(right=False)
            if event.key == pygame.K_UP:
                in_state = in_state._replace(up=False)
            if event.key == pygame.K_DOWN:
                in_state = in_state._replace(down=False)
            if event.key == pygame.K_x:
                in_state = in_state._replace(inv_left=False)
            if event.key == pygame.K_PERIOD:
                in_state = in_state._replace(inv_right=False)
            if event.key == pygame.K_q:
                in_state = in_state._replace(inv_up=False)
            if event.key == pygame.K_g:
                in_state = in_state._replace(inv_down=False)

    return in_state
